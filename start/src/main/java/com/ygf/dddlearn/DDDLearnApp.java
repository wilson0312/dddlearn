package com.ygf.dddlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author ygf
 * @Date 2021/3/10 23:20
 * @Description
 */
@SpringBootApplication
public class DDDLearnApp {

    public static void main(String[] args) {
        SpringApplication.run(DDDLearnApp.class, args);
    }
}
